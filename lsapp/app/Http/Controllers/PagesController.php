<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Post;

class PagesController extends Controller
{
    public function index(){
        //$title = 'Welcome To Laravel Bro';
        //return view('show.index', compact('title'));

        //$posts = DB::table('posts')->latest()->first();
        $posts = Post::orderBy('created_at','desc')->paginate(3);
        //$posts = Post::latest()->first();
        return view('show.index')->with('posts',$posts);
        //return view('show.index');
    }

    public function about(){
        $title = 'About Us';
        return view('pages.about') -> with('title', $title) ;
    }

    public function services(){
        $data = array(
            'title' => 'Services',
            'services' => ['Web Design', 'Programming', 'SEO']
        );
        return view('pages.services')->with($data);
    }
}

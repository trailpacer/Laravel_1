<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    //return view('welcome');
    return '<h1>Hello World</h1>';
});

Route::get('/users/{id}', function($id){
    return 'This is user'. $id;
});
*/

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');

/*
Route::get('/about', function(){
    return view('pages.about');
});
*/

Route::resource('posts', 'PostController');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');

Route::group(['prefix' => 'mlebu'], function(){
	Route::get('/', 'DashboardController@index');
	Route::get('/read/{id}', 'DashboardController@show');
	Route::get('/add', 'DashboardController@create');
	Route::post('/store', 'DashboardController@store');
	Route::get('/edit/{id}', 'DashboardController@edit');
	Route::post('/update/{id}', 'DashboardController@update');
	Route::get('/delete/{id}', 'DashboardController@destroy');
});